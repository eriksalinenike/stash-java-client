package com.atlassian.stash.rest.client.core.http;

import com.atlassian.stash.rest.client.api.StashException;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

public interface HttpExecutor {
    /**
     * Executes requests to get response
     *
     * @param httpRequest Request to be executed
     * @return Response from request execution. Executor should always return response regardless http status code
     *
     * @throws StashException Thrown in case of non http error eg. connection problems, dns resolution problems, etc.
     */
    @Nullable
    <T> T execute(@Nonnull HttpRequest httpRequest, @Nonnull HttpResponseProcessor<T> responseProcessor) throws StashException;
}
