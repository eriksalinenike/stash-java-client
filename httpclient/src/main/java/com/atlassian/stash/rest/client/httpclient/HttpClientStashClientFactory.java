package com.atlassian.stash.rest.client.httpclient;

import com.atlassian.stash.rest.client.api.StashClient;

import javax.annotation.Nonnull;

public interface HttpClientStashClientFactory {

    StashClient getStashClient(@Nonnull HttpClientConfig config);

}
