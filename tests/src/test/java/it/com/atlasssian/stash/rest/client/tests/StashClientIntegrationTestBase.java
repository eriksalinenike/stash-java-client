package it.com.atlasssian.stash.rest.client.tests;

import com.atlassian.stash.rest.client.api.StashClient;
import com.atlassian.stash.rest.client.api.StashUnauthorizedRestException;
import com.atlassian.stash.rest.client.api.entity.Branch;
import com.atlassian.stash.rest.client.api.entity.Page;
import com.atlassian.stash.rest.client.api.entity.Permission;
import com.atlassian.stash.rest.client.api.entity.Project;
import com.atlassian.stash.rest.client.api.entity.Repository;
import com.atlassian.stash.rest.client.api.entity.Tag;
import com.google.common.base.Function;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.junit.Before;
import org.junit.Test;

import java.util.List;
import java.util.Map;
import java.util.Set;

import static com.atlassian.stash.rest.client.api.EntityMatchers.branch;
import static com.atlassian.stash.rest.client.api.EntityMatchers.page;
import static com.atlassian.stash.rest.client.api.EntityMatchers.project;
import static com.atlassian.stash.rest.client.api.EntityMatchers.repository;
import static com.atlassian.stash.rest.client.api.EntityMatchers.tag;
import static it.com.atlasssian.stash.rest.client.tests.TestUtil.STASH_ADMIN_LOGIN;
import static it.com.atlasssian.stash.rest.client.tests.TestUtil.STASH_ADMIN_PASSWORD;
import static it.com.atlasssian.stash.rest.client.tests.TestUtil.STASH_USER_LOGIN;
import static it.com.atlasssian.stash.rest.client.tests.TestUtil.STASH_USER_PASSWORD;
import static it.com.atlasssian.stash.rest.client.tests.TestUtil.generateSshKey;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

public abstract class StashClientIntegrationTestBase {
    // test set up
    protected StashClient service;


    @Before
    public void setUp() throws Exception {
        service = createStashClient(STASH_ADMIN_LOGIN, STASH_ADMIN_PASSWORD);
    }

    abstract protected StashClient createStashClient(String stashUsername, String stashPassword) throws Exception;

    @Test
    public void testGetAccessibleProjects() {
        // when
        Page<Project> projectPage = service.getAccessibleProjects(0, 10);

        // then
        assertThat(projectPage, page(Project.class).size(is(2)).build());
        List<Project> projects = Lists.newArrayList(projectPage.getValues());
        assertThat(projects.get(0), project()
                .key(is("BAM"))
                .name(is("Bamboo"))
                .description(is("Bamboo functional test data"))
                .selfUrl(is("http://localhost:7990/stash/projects/BAM"))
                .build()
        );
    }

    @Test
    public void testGetRepositories() {
        // when
        Page<Repository> repositoryPage = service.getRepositories(null, null, 0, 10);

        // then
        assertThat(repositoryPage, page(Repository.class).size(is(2)).build());

        List<Repository> repositories = Lists.newArrayList(repositoryPage.getValues());
        assertThat(repositories.get(0), repository()
                .slug(is("rep_1"))
                .name(is("rep_1"))
                .sshCloneUrl(is("ssh://git@localhost:7999/project_1/rep_1.git"))
                .httpCloneUrl(is("http://admin@localhost:7990/stash/scm/project_1/rep_1.git"))
                .selfUrl(is("http://localhost:7990/stash/projects/PROJECT_1/repos/rep_1/browse"))
                .project(project()
                                .selfUrl(is("http://localhost:7990/stash/projects/PROJECT_1"))
                                .build()
                )
                .build());
        assertThat(repositories.get(1), repository()
                .slug(is("sample-projects"))
                .name(is("Sample projects"))
                .sshCloneUrl(is("ssh://git@localhost:7999/bam/sample-projects.git"))
                .httpCloneUrl(is("http://admin@localhost:7990/stash/scm/bam/sample-projects.git"))
                .selfUrl(is("http://localhost:7990/stash/projects/BAM/repos/sample-projects/browse"))
                .project(project()
                        .selfUrl(is("http://localhost:7990/stash/projects/BAM"))
                        .build())
                .build());
    }

    @Test
    public void testGetRepositoriesWithSearch() {
        // when
        Page<Repository> repositoryPage = service.getRepositories(null, "sam", 0, 10);

        // then
        assertThat(repositoryPage, page(Repository.class).size(is(1)).build());

        List<Repository> repositories = Lists.newArrayList(repositoryPage.getValues());
        assertThat(repositories.get(0), repository()
                .slug(is("sample-projects"))
                .build());
    }

    @Test
    public void testGetRepository() {
        // when
        Repository repository = service.getRepository("project_1", "rep_1");

        // then
        assertThat(repository, repository()
                .slug(is("rep_1"))
                .name(is("rep_1"))
                .sshCloneUrl(is("ssh://git@localhost:7999/project_1/rep_1.git"))
                .httpCloneUrl(is("http://admin@localhost:7990/stash/scm/project_1/rep_1.git"))
                .selfUrl(is("http://localhost:7990/stash/projects/PROJECT_1/repos/rep_1/browse"))
                .project(project()
                                .selfUrl(is("http://localhost:7990/stash/projects/PROJECT_1"))
                                .build()
                )
                .build());
    }

    @Test
    public void testGetRepositoryIfNotExists() {
        // when
        Repository repository = service.getRepository("project_1", "NON_EXISTING_REPO");

        // then
        assertThat(repository, nullValue());
    }

    @Test
    public void testAddRepositoryKey() {
        // given
        TestUtil.SshKeyPair keyPair = generateSshKey(1024, "some label");
        boolean isKey = service.isRepositoryKey("BAM", "sample-projects", keyPair.getPublicKey());
        assertThat("key should NOT exist before adding", isKey, is(false));

        // when
        boolean result = service.addRepositoryKey("BAM", "sample-projects", keyPair.getPublicKey(), null, Permission.REPO_READ);

        // then
        assertThat("add call should be successful", result, is(true));
        isKey = service.isRepositoryKey("BAM", "sample-projects", keyPair.getPublicKey());
        assertThat("key should exist after adding", isKey, is(true));
    }

    @Test
    public void testAddUserKey() {
        // given
        TestUtil.SshKeyPair keyPair = generateSshKey(1024, "some label");
        boolean isKey = service.isUserKey(keyPair.getPublicKey());
        assertThat("key should NOT exist before adding", isKey, is(false));

        // when
        boolean result = service.addUserKey(keyPair.getPublicKey(), null);

        // then
        assertThat("add call should be successful", result, is(true));
        isKey = service.isUserKey(keyPair.getPublicKey());
        assertThat("key should exist after adding", isKey, is(true));
    }

    @Test
    public void testFallbackAddRepositoryKeyToAddUserKey() {
        // given
        try {
            service = createStashClient(STASH_USER_LOGIN, STASH_USER_PASSWORD);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        TestUtil.SshKeyPair keyPair = generateSshKey(1024, "some label");

        // when
        try {
            service.addRepositoryKey("BAM", "sample-projects", keyPair.getPublicKey(), null, Permission.REPO_READ);
            fail("add ssh key to repository with user 'user' should fail");
        } catch(StashUnauthorizedRestException e) {
            // expected exception
        } catch(Throwable e) {
            fail("only StashUnauthorizedRestException should be thrown");
        }
        boolean addUserResult = service.addUserKey(keyPair.getPublicKey(), null);

        // then
        assertThat("add call should be successful", addUserResult, is(true));
        boolean isKey = service.isUserKey(keyPair.getPublicKey());
        assertThat("key should exist after adding", isKey, is(true));
    }

    @Test
    public void testGetRepositoryBranches() {
        // when
        Page<Branch> branchPage =
                service.getRepositoryBranches("BAM", "sample-projects", null, 0, 10);

        // then
        List<Branch> branches = Lists.newArrayList(branchPage.getValues());
        Map<String, Branch> branchMap = Maps.uniqueIndex(branches, STASH_BRANCH_ENTITY_TO_NAME);
        assertThat(branchMap.keySet(), is((Set<String>) ImmutableSet.of("master", "feature-branch-x", "feature-branch-y",
                "branch_no_1", "branch_no_2")));
        assertThat(branchMap.get("master"), branch()
                .displayId(is("master"))
                .latestChangeset(is("9e8bd5114cd06c857a89651c329c1142a024f956"))
                .isDefault(is(true))
                .build()
        );
    }

    @Test
    public void testGetRepositoryTags() {
        // when
        Page<Tag> tag =
                service.getRepositoryTags("PROJECT_1", "rep_1", null, 0, 10);

        // then
        List<Tag> tags = Lists.newArrayList(tag.getValues());
        Map<String, Tag> tagMap = Maps.uniqueIndex(tags, STASH_TAG_ENTITY_TO_NAME);
        assertThat(tagMap.keySet(), is((Set<String>) ImmutableSet.of("retagged_signed_tag", "signed_tag", "backdated_annotated_tag")));

        assertThat(tagMap.get("signed_tag"), tag()
                .displayId(is("signed_tag"))
                .id(is("refs/tags/signed_tag"))
                .latestChangeset(is("0a943a29376f2336b78312d99e65da17048951db"))
                .hash(is("12ebe2a58367347cd39f19f5a72f3cbec7b8f9a9"))
                .build()
        );
    }

    @Test
    public void testGetRepositoryDefaultBranch() {
        // when
        Branch branch = service.getRepositoryDefaultBranch("BAM", "sample-projects");

        // then
        assertThat(branch, branch()
                .displayId(is("master"))
                .latestChangeset(is("9e8bd5114cd06c857a89651c329c1142a024f956"))
                .isDefault(is(true))
                .build()
        );
    }

    @Test
    public void testGetRepositoryBranchesWithSearch() {
        // when
        Page<Branch> branchPage =
                service.getRepositoryBranches("BAM", "sample-projects", "feature-", 0, 10);

        // then
        List<Branch> branches = Lists.newArrayList(branchPage.getValues());
        Map<String, Branch> branchMap = Maps.uniqueIndex(branches, STASH_BRANCH_ENTITY_TO_NAME);
        assertThat(branchMap.keySet(), is((Set<String>) ImmutableSet.of("feature-branch-x", "feature-branch-y")));
    }

    public static final Function<Branch,String> STASH_BRANCH_ENTITY_TO_NAME = new Function<Branch, String>() {
        @Override
        public String apply(Branch input) {
            return input.getDisplayId();
        }
    };

    public static final Function<Tag,String> STASH_TAG_ENTITY_TO_NAME = new Function<Tag, String>() {
        @Override
        public String apply(Tag input) {
            return input.getDisplayId();
        }
    };

}
